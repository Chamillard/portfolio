import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

Vue.use(Buefy)

import Default from "./layouts/Default.vue";
import Home from "./layouts/Home.vue";

Vue.component("default-layout", Default);
Vue.component("home-layout", Home);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
