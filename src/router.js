import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      meta: { layout: "home" },
      component: require("@/pages/Accueil.vue").default
    },
    {
      path: "/competences",
      name: "competences",
      component: () => import("@/pages/Competences.vue")
    },
    {
      path: "/formations",
      name: "formations",
      component: () => import("@/pages/Formations.vue")
    },
    {
      path: "/experiences",
      name: "experiences",
      component: () => import("@/pages/Experiences.vue")
    }
  ]
})
